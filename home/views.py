from django.shortcuts import render, HttpResponse
from .models import Contact
from django.contrib import messages


# Create your views here.
def index(request):
    # return HttpResponse("This is my home page")
    return render(request, 'index.html')


def about(request):
    # return HttpResponse("This is my about page")
    return render(request, 'about.html')


def services(reqeust):
    # return HttpResponse("This is my services page")
    return render(reqeust, 'services.html')


def contact(request):  # request is a dictionary here
    if request.method == "POST":
        name = request.POST["name"]
        email = request.POST["email"]
        phone = request.POST["phone"]
        desc = request.POST["desc"]
        contact = Contact(name=name, email=email, phone=phone, desc=desc)
        contact.save()
        messages.success(request, 'Your message has been sent.')
    # return HttpResponse("This is my contact page")
    return render(request, 'contact.html')
