from django.db import models


# Create your models here.
class Contact(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=10)
    desc = models.TextField()

    def __str__(
            self):  # will show names in the admin panel instead of contact object1,contact object2...contact objectn
        return self.name
